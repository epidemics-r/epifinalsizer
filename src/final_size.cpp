// [[Rcpp::depends(RcppEigen)]]

#include "epiFinalSizeR_types.hpp"

#include <pagmo/algorithm.hpp>
#include <pagmo/algorithms/sade.hpp>
#include <pagmo/population.hpp>
#include <pagmo/problem.hpp>

using namespace pagmo;

struct problem_function {
  std::pair<vector_double, vector_double> bounds;
  std::function<vector_double(const vector_double &v)> fitness_f;

  auto fitness(const vector_double &v) const { return fitness_f(v); }

  auto get_bounds() const { return bounds; }
};

double logit(double p) { return -log(1.0 / p - 1.0); }

//' Logisitic transform
//'
//' @param x The x value
//'
//' @export
// [[Rcpp::export]]
double logistic(double x) {
  auto e = exp(x);
  return e / (e + 1);
}

//' Build matrix from its compartments
//'
//' A(i,j) = b*c(i,j)*i(j)*s(i)
//'
//' @param transmissibility b
//' @param contact_matrix c: probability random individual stratus $i$ meets
//'    random individual stratus $j$. Note this is a symmetric matrix. ' 
//' @param infectiousness i
//' @param susceptibility s 
//'
//' @export
// [[Rcpp::export]]
Eigen::MatrixXd build_matrix(const double transmissibility,
                             const Eigen::MatrixXd &contact_matrix,
                             const Eigen::VectorXd &infectiousness,
                             const Eigen::VectorXd &susceptibility) {
  auto base_matrix = contact_matrix;
  for (size_t i = 0; i < contact_matrix.rows(); ++i) {
    for (size_t j = 0; j < contact_matrix.cols(); ++j) {
      base_matrix(i, j) = transmissibility * contact_matrix(i, j) *
                          infectiousness(j) * susceptibility(i);
    }
  }
  return base_matrix;
}

//' Setup model state from its compartments
//'
//' @export
// [[Rcpp::export]]
ModelState build_model_state(const ModelState& state) {
  return state;
}

std::vector<double> pagmo_optimise_with_prior(
    std::function<std::vector<double>(const std::vector<double> &)> f,
    std::function<std::vector<double>()> prior,
    std::vector<double> lower, std::vector<double> upper, double tolerance) {
  auto problem_definition = problem_function();

  problem_definition.bounds = {lower, upper};
  problem_definition.fitness_f = f;

  problem prob{problem_definition};

  // 2 - Instantiate a pagmo algorithm
  algorithm algo{sade(1000000, 2, 1, tolerance, tolerance)};
  size_t dim = upper.size();

  // Half random draws and half from the prior
  auto pop_size = std::max<size_t>(50, dim * 2);
  population pop{prob, pop_size};
  for (auto i = 0; i < pop_size; ++i) {
    pop.push_back(std::move(prior()));
  }

  // 4 - Evolve the population
  pop = algo.evolve(pop);

  // 5 - Output the population
  // Rcpp::Rcout << "The population: \n" << pop;
  return pop.champion_x();
}

std::vector<double> pagmo_optimise(
    std::function<std::vector<double>(const std::vector<double> &)> f,
    std::vector<double> lower, std::vector<double> upper, double tolerance) {
  auto problem_definition = problem_function();

  problem_definition.bounds = {lower, upper};
  problem_definition.fitness_f = f;

  problem prob{problem_definition};

  // 2 - Instantiate a pagmo algorithm
  algorithm algo{sade(1000000, 2, 1, tolerance, tolerance)};
  size_t dim = upper.size();
  population pop{prob, std::max<size_t>(100, dim * 4)};

  // 4 - Evolve the population
  pop = algo.evolve(pop);

  // 5 - Output the population
  // Rcpp::Rcout << "The population: \n" << pop;
  return pop.champion_x();
}

//' Optimise a function using pagmo
//'
//' @export
// [[Rcpp::export]]
std::vector<double> pagmo_optimise(Rcpp::Function f, std::vector<double> lower,
                                   std::vector<double> upper,
                                   double tolerance) {
  auto func = [&f](const std::vector<double> &v) {
    PutRNGstate();
    auto ret = Rcpp::as<std::vector<double>>(f(v));
    GetRNGstate();
    return ret;
  };

  return pagmo_optimise(func, lower, upper, tolerance);
}

//' Optimise a function using pagmo
//'
//' @export
// [[Rcpp::export]]
std::vector<double> pagmo_optimise_with_prior(Rcpp::Function f,
                                              Rcpp::Function prior,
                                              std::vector<double> lower,
                                              std::vector<double> upper,
                                              double tolerance) {
  auto func = [&f](const std::vector<double> &v) {
    PutRNGstate();
    auto ret = Rcpp::as<std::vector<double>>(f(v));
    GetRNGstate();
    return ret;
  };

  auto prior_func = [&prior]() {
    PutRNGstate();
    auto ret = Rcpp::as<std::vector<double>>(prior());
    GetRNGstate();
    return ret;
  };

  return pagmo_optimise_with_prior(func, prior_func, lower, upper, tolerance);
}

//' Combine matrix with demography 
//'
//' @export
// [[Rcpp::export]]
Eigen::MatrixXd combine(const ModelState &state) {
  auto m = state.M;
  for (size_t i = 0; i < state.M.rows(); ++i) {
    for (size_t j = 0; j < state.M.cols(); ++j) {
      m(i,j) *= state.N(j);
    }
  };
  return m;
};

//' Add vaccination groups
//'
//' @export
// [[Rcpp::export]]
ModelState add_vaccination(const ModelState &state, const Eigen::VectorXd fraction, 
    const Eigen::VectorXd efficacy, bool leaky = false) {
  auto dim = state.N.size();
  ModelState vaccState;
  vaccState.M = Eigen::MatrixXd(2*dim, 2*dim);
  vaccState.N = Eigen::VectorXd(2*dim);

  auto one_v = Eigen::ArrayXd::Ones(dim);
  
  for (size_t i = 0; i < 2; ++i) {
    for (size_t j = 0; j < 2; ++j) {
      vaccState.M.block(i*dim, j*dim, dim, dim) = state.M;
    }
  }
  if (!leaky) {
    auto fr = fraction.array()*efficacy.array();
    vaccState.N.block(0, 0, dim, 1) = (one_v - fr.array())*state.N.array();
    vaccState.N.block(dim, 0, dim, 1) = fr.array()*state.N.array();

    for (size_t i = 0; i < dim; ++i) {
      for (size_t j = 0; j < 2*dim; ++j)  {
        vaccState.M(dim + i,j) *= 0;
      }
    }
  } else {
    vaccState.N.block(0, 0, dim, 1) = (one_v - fraction.array())*state.N.array();
    vaccState.N.block(dim, 0, dim, 1) = fraction.array()*state.N.array();

    for (size_t i = 0; i < dim; ++i) {
      for (size_t j = 0; j < 2*dim; ++j)  {
        vaccState.M(dim + i,j) *= (1-efficacy(i));
      }
    }
  }

  return initialise_state(std::move(vaccState));
}

//' Calculate final size
//'
//' @export
// [[Rcpp::export]]
Eigen::VectorXd final_size_cpp(const ModelState &state) {
  auto tolerance = 1e-8;

  auto nDim = state.dim - state.nZeros;
  auto lower = vector_double(nDim, 0);
  auto upper = vector_double(nDim, 1);

  auto m = combine(state);

  auto f = [&state, &m](const vector_double &v) {
    vector_double fitness(1, 0);
    auto k = 0;
    for (size_t i = 0; i < m.rows(); ++i) {
      if (state.isZero(i))
        continue;
      double row_sum = 0;
      auto l = 0;
      for (size_t j = 0; j < m.cols(); ++j) {
        if (state.isZero(j))
          continue;
        row_sum += m(i, j) * v[l];
        ++l;
      }
      // Multiple by two, because the error is likely halfway between the new
      // prediction and the old
      fitness[0] += 2 * abs(1 - exp(-row_sum) - v[k]);
      ++k;
    }
    return fitness;
  };

  Eigen::VectorXd v(state.N.size());
  auto result = pagmo_optimise(f, lower, upper, tolerance);
  auto k = 0;
  for (auto i = 0; i < v.size(); ++i) {
    if (state.isZero(i)) {
      v(i) = 0;
      continue;
    }
    v(i) = result[k];
    if (result[k] < tolerance) v(i) = 0;
    ++k;
  }

  return v;
}

//' Calculate derivative of final size to changes in population size
//'
//' Note we require the vaccinated groups to be included. You can use 
//' add_vaccination to create the correct type state.
//'
//' @param index The group that changes, note that the index starts at zero
//' inline with cpp convention
//'
//' @export
// [[Rcpp::export]]
Eigen::VectorXd final_size_derivative_cpp(const Eigen::VectorXd &pi,
                                          const ModelState &state,
                                          size_t index) {
  // v*(I*-A)^-1
  auto dim = state.N.size();
  Eigen::VectorXd v(dim);
  Eigen::MatrixXd a(dim, dim);
  auto ident = Eigen::MatrixXd::Identity(dim, dim);

  // Fill v
  for (size_t i = 0; i < v.size(); ++i)
    v(i) =
        (1 - pi(i)) * (pi(index) * state.M(i, index) -
          // This assumes that the vaccinated groups are at the end
          pi(index + dim/2) * state.M(i, index + dim/2));

  // Fill a
  for (size_t i = 0; i < v.size(); ++i) {
    for (size_t j = 0; j < v.size(); ++j) {
      a(i, j) =
          (1 - pi(i)) * state.M(i, j) * state.N(j);
    }
  }

  return ((ident - a).inverse()) * v;
}

//' Calculate the impact based on pi and derivative
//'
//' @export
// [[Rcpp::export]]
Rcpp::List vaccination_impact_from_derivative_cpp(
    const ModelState &state, const Eigen::VectorXd &pi,
    const Eigen::VectorXd &derivative, const Eigen::VectorXd &efficacy,
    size_t index, bool leaky = false) {
  Rcpp::List lst(0);
  auto dim = state.N.size()/2;

  if (!leaky) {
    lst["indirect"] = efficacy(index) * (derivative.array() * state.N.array());
    lst["direct"] = efficacy(index) * (pi[index] - pi[index + dim]);
  } else {
    lst["indirect"] = (derivative.array() * state.N.array());
    lst["direct"] = (pi[index] - pi[index + dim]);
  }
  return lst;
}

//' Calculate impact of one additional vaccination 
//'
//' @param index The group that changes, note that the index starts at zero
//' inline with cpp convention
//'
//' @export
// [[Rcpp::export]]
Rcpp::List vaccination_impact_cpp(const ModelState &state, const Eigen::VectorXd &fraction, 
    const Eigen::VectorXd &efficacy, size_t index, bool leaky = false) {
  auto dim = state.N.size();
  auto s = add_vaccination(state, fraction, efficacy, leaky);
  auto pi = final_size_cpp(s);
  //auto der = final_size_derivative_cpp(pi, s, index).array()*s.N.array();
  auto der = final_size_derivative_cpp(pi, s, index);

  return vaccination_impact_from_derivative_cpp(s, pi, der, efficacy, index, leaky);
}


