#ifndef EPI_FINAL_SIZE_HPP
#define EPI_FINAL_SIZE_HPP

#include <RcppEigen.h>

struct ModelState {
  // M[i,j] probability that random j individual infects i
  Eigen::MatrixXd M;
  // N[i] population size of group i
  Eigen::VectorXd N;

  Eigen::VectorXi isZero;
  size_t dim = 0;
  size_t nZeros = 0;
};

inline ModelState initialise_state(ModelState && state) {
  state.isZero = Eigen::VectorXi(state.N.size());

  state.dim = state.N.size();

  for (size_t i = 0; i < state.N.size(); ++i) {
    state.isZero(i) = (state.M.row(i).maxCoeff() == 0);
    if (state.isZero(i))
      ++state.nZeros;
  }
  return state;
}

namespace Rcpp {
template <>
inline SEXP wrap(const ModelState &state) {
  Rcpp::List RState(0);
  RState["M"] = state.M;
  RState["N"] = state.N;

  RState[".internal.isZero"] = state.isZero;
  RState[".internal.nZeros"] = state.nZeros;
  return Rcpp::wrap(RState);
}

template <>
inline ModelState as(SEXP dataRObj) {
  ModelState state;
  auto RState = Rcpp::as<Rcpp::List>(dataRObj);
  state.M = RState["M"];
  state.N = RState["N"];
  state = initialise_state(std::move(state));
  return state;
}
};  // namespace Rcpp

#include <Rcpp.h>

#endif
